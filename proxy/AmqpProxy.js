let amqpConection;
let amqpChannel;
module.exports = class AmqpProxy {
	constructor(urlAmqpServer) {

		this.url = urlAmqpServer;
		this.amqp = require('amqplib/callback_api');

		if (this.url === undefined || this.url == null) {
			this.url = 'amqp://localhost'

		}
		if (!this.url.startsWith("amqp://")) {
			console.log("error: Falha ao construir proxy-amqp, url do servidor invalida!");
			return

		}

		estabelecerConexao(this.amqp, this.url);
	}


	writeMessageOnQueue(queue, msg, options) {
		msg = new Buffer(JSON.stringify(msg));
		amqpChannel.assertQueue(queue, {durable: false});
		amqpChannel.sendToQueue(queue, msg, options);
	}

	consumirFila(queue, callback) {
		amqpChannel.assertQueue(queue, {durable: false});
		amqpChannel.consume(queue, function (msg) {
			callback(queue, JSON.parse(msg.content.toString()));
		}, {noAck: true});
	}

	getChannel() {
		return amqpChannel;
	}

	deleteQueue(queue) {
		this.amqp.deleteQueue(queue);
	}
};

function estabelecerConexao(amqp, url) {
	amqp.connect(url, function (err, conn) {
		conn.createChannel(function (err, ch) {
			amqpChannel = ch;
			amqpConection = conn;
		});
		setTimeout(function () {
			conn.close();
			process.exit(0)
		}, 500);
	});
}