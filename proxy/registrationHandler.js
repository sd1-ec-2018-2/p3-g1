const irc = require('irc');
const setupIrcListeners = require('./eventHandler');

module.exports = function (clients, amqp) {
	amqp.channel.assertQueue('registration', {durable: false});
	amqp.channel.consume('registration', function (msg) {
		let client, data = JSON.parse(msg.content.toString()), id = data.id;

		if (id) {
			if (clients.hasOwnProperty(id))
				client = clients[id];
			else
				return amqp.channel.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify({
					type: 'error',
					err: 'invalid id'
				})));
		} else {
			const crypto = require("crypto");
			do {
				id = crypto.randomBytes(16).toString("hex");
			} while (clients[id]);

			client = obtenhaClient(data);

			client.close = function (message) {
				amqp.channel.deleteQueue(this.queue);
				this.queue = 'trash';
				this.irc.disconnect(message);
				delete clients[id];
			};

			clients[id] = client;
			setupIrcListeners(client, amqp);
		}

		amqp.channel.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify({
			type: 'registration',
			data: {
				id: id,
				nick: client.nick,
				servidor: client.servidor,
				manterAtivo: client.manterAtivo,
				canais: client.canais,
				prefixCanal: client.irc.opt.channelPrefixes,
				registered: client.registered
			}
		})));
	}, {noAck: true});
};

function obtenhaClient(data){
	return {
		queue: 'user_' + id,
		nick: data.nome,
		servidor: data.servidor,
		manterAtivo: data.manterAtivo,
		canais: [],
		registered: false,

		irc: new irc.Client(data.servidor, data.nome, {
			userName: data.nome,
			realName: data.nome,
			channels: [data.canal]
		})
	};
}
