let comandos = {};
module.exports = class IrcProxy {
	constructor(amqp) {

		this.amqp = amqp;
		this.irc = require('irc');
		this.ircClients = {};

		registration(this.ircClients, this.amqp);

		this.amqp.consumirFila('irc', function (data) {
			let id = data.id,
				client = ircClients[id];
			if (!client) return;

			if (comandos.hasOwnProperty(data.command))
				comandos[data.command](client, data.data);
		});
	}
};

function registration(ircClients, amqpProxy) {
	let channel = amqpProxy.getChannel();
	channel.assertQueue('registration', {durable: false});
	channel.consume('registration', function (msg) {
		let client;
		let data = JSON.parse(msg.content.toString());
		if (data.id) {

			if (ircClients.hasOwnProperty(data.id)) {
				client = ircClients[data.id];
			} else {
				return amqpProxy.getChannel().sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify({
					type: 'error',
					err: 'invalid id'
				})));
			}

		} else {

			let newID = obtenhaNewID(ircClients);
			client = {
				queue: 'user_' + newID,
				nick: data.nome,
				servidor: data.servidor,
				manterAtivo: data.manterAtivo,
				canais: [],
				registered: false,

				irc: new irc.Client(data.servidor, data.nome, {
					userName: data.nome,
					realName: data.nome,
					channels: [data.canal]
				}),

				close: function (message) {
					amqpProxy.deleteQueue(this.queue);
					this.queue = 'trash';
					this.irc.disconnect(message);
					delete ircClients[id];
				}
			};

			ircClients[newID] = client;
			setEvents(client, amqpProxy)
		}

		amqpProxy.getChannel().sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify({
			type: 'registration',
			data: {
				id: id,
				nick: client.nick,
				servidor: client.servidor,
				manterAtivo: client.manterAtivo,
				canais: client.canais,
				prefixCanal: client.irc.opt.channelPrefixes,
				registered: client.registered
			}
		})));
	}, {noAck: true});

}


function obtenhaNewID(ircClients) {
	const crypto = require("crypto");
	let id;
	do {
		id = crypto.randomBytes(16).toString("hex");
	} while (ircClients[id]);
	return id;
}

function setEvents(client, amqp) {
	let ircObj = client.irc;
	ircObj.addListener('registered', function (message) {
		client.registered = true;
		sendIrcEvent(amqp, client.queue, 'registered');
	});

	ircObj.addListener('error', function (message) {
		sendIrcEvent(amqp, client.queue, 'irc error', {
			text: message.args[message.args.length - 1]
		});
	});

	ircObj.addListener('message#', function (nick, to, text, message) {
		sendIrcEvent(amqp, client.queue, 'message', {
			from: nick,
			to: to,
			text: text
		});
	});

	ircObj.addListener('pm', function (nick, text, message) {
		sendIrcEvent(amqp, client.queue, 'pm', {
			from: nick,
			text: text
		});
	});

	ircObj.addListener('join', function (channel, nick, message) {
		if (nick === client.nick)
			client.canais.push(channel);

		sendIrcEvent(amqp, client.queue, 'join', {
			channel: channel,
			nick: nick
		});
	});

	ircObj.addListener('part', function (channel, nick, reason, message) {
		if (nick === client.nick)
			client.canais.splice(client.canais.indexOf(channel), 1);

		sendIrcEvent(amqp, client.queue, 'part', {
			channel: channel,
			nick: nick,
			reason: reason
		});
	});

	ircObj.addListener('quit', function (nick, reason, channels, message) {
		sendIrcEvent(amqp, client.queue, 'quit', {
			nick: nick,
			reason: reason,
			channels: channels
		});
	});

	ircObj.addListener('whois', function (data) {
		sendIrcEvent(amqp, client.queue, 'whois', data);
	});

	ircObj.addListener('motd', function (data) {
		sendIrcEvent(amqp, client.queue, 'motd', data);
	});

	ircObj.addListener('topic', function (channel, topic, nick, message) {
		sendIrcEvent(amqp, client.queue, 'topic', {
			nick: nick,
			topic: topic,
			channel: channel
		});
	});

	ircObj.addListener('kick', function (channel, nick, by, reason, message) {
		sendIrcEvent(amqp, client.queue, 'kick', {
			channel: channel,
			nick: nick,
			reason: reason,
			by: by
		});
	});

	ircObj.addListener('nick', function (oldnick, newnick, channels, message) {
		sendIrcEvent(amqp, client.queue, 'nick', {
			oldnick: oldnick,
			newnick: newnick,
			channels: channels
		});
	});

	ircObj.addListener('names', function (channel, nicks) {
		sendIrcEvent(amqp, client.queue, 'names', {
			channel: channel,
			nicks: Object.keys(nicks),
			modifiers: nicks
		});
	});

	ircObj.addListener('kill', function (nick, reason, channels, message) {
		sendIrcEvent(amqp, client.queue, 'kill', {
			nick: nick,
			reason: reason,
			channels: channels
		});
	});

	ircObj.addListener('invite', function (channel, from, message) {
		sendIrcEvent(amqp, client.queue, 'invite', {
			channel: channel,
			from: from
		});
	});

	ircObj.addListener('action', function (from, to, text, message) {
		sendIrcEvent(amqp, client.queue, 'action', {
			from: from,
			to: to,
			text: text
		});
	});

	ircObj.addListener('+mode', function (channel, by, mode, argument, message) {
		sendIrcEvent(amqp, client.queue, 'mode', {
			channel: channel,
			by: by,
			mode: '+' + mode,
			argument: argument
		});
	});

	ircObj.addListener('-mode', function (channel, by, mode, argument, message) {
		sendIrcEvent(amqp, client.queue, 'mode', {
			channel: channel,
			by: by,
			mode: '-' + mode,
			argument: argument
		});
	});

	ircObj.addListener('channellist_start', function (message) {
		sendIrcEvent(amqp, client.queue, 'channellist_start');
	});

	ircObj.addListener('channellist_item', function (channel) {
		sendIrcEvent(amqp, client.queue, 'channellist_item', channel);
	});

	ircObj.addListener('notice', function (nick, to, text, message) {
		if (!nick) nick = 'Server';

		sendIrcEvent(amqp, client.queue, 'notice', {
			from: nick,
			to: to,
			text: text
		});
	});
}

function sendIrcEvent(amqp, queue, event, data) {
	amqp.writeMessageOnQueue(queue, {
		type: 'irc event',
		event: event,
		data: data
	});
}


comandos.disconnect = function (client) {
	if (!client.manterAtivo)
		client.close();
};

comandos.quit = function (client, data) {
	client.close(data);
};

comandos.message = function (client, data) {
	client.irc.say(data.to, data.message);
};

comandos.join = function (client, data) {
	client.irc.join(data);
};

comandos.part = function (client, data) {
	client.irc.part(data.canal, data.mensagem);
};

comandos.whois = function (client, data) {
	client.irc.whois(data);
};

comandos.names = function (client, data) {
	client.irc.writeONQueue('names', data.channel);
};

comandos.nick = function (client, data) {
	client.irc.writeONQueue('nick', data);
};

comandos.action = function (client, data) {
	client.irc.action(data.target, data.message);
};

comandos.motd = function (client) {
	client.irc.writeONQueue('motd');
};

comandos.mode = function (client, data) {
	client.irc.writeONQueue('mode', ...data);
};

comandos.list = function (client) {
	client.irc.list();
};

comandos.notice = function (client, data) {
	client.irc.notice(data.to, data.message);
};