let comandos = {};

comandos.disconnect = function (proxy) {
	if (!proxy.manterAtivo)
		proxy.close();
};

comandos.message = function (proxy, data) {
	proxy.irc.say(data.to, data.message);
};

comandos.quit = function (proxy, data) {
	proxy.close(data);
};

comandos.join = function (proxy, data) {
	proxy.irc.join(data);
};

comandos.part = function (proxy, data) {
	proxy.irc.part(data.canal, data.mensagem);
};

comandos.names = function (proxy, data) {
	proxy.irc.writeONQueue('names', data.channel);
};

comandos.nick = function (proxy, data) {
	proxy.irc.writeONQueue('nick', data);
};

comandos.whois = function (proxy, data) {
	proxy.irc.whois(data);
};

comandos.action = function (proxy, data) {
	proxy.irc.action(data.target, data.message);
};

comandos.motd = function (proxy) {
	proxy.irc.writeONQueue('motd');
};

comandos.mode = function (proxy, data) {
	proxy.irc.writeONQueue('mode', ...data);
};


comandos.notice = function (proxy, data) {
	proxy.irc.notice(data.to, data.message);
};

comandos.list = function (proxy) {
	proxy.irc.list();
};


module.exports = function (clients, amqp) {
	amqp.channel.assertQueue('irc', {durable: false});
	amqp.channel.consume('irc', function (msg) {
		let message = JSON.parse(msg.content.toString()),
			id = message.id,
			proxy = clients[id];

		if (!proxy) return;

		if (comandos.hasOwnProperty(message.command))
			comandos[message.command](proxy, message.data);
	}, {noAck: true});
};