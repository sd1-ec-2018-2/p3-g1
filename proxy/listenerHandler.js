module.exports = function (client) {
	let socket = client.socket;
	let amqp = client.amqp;

	socket.on('quit', function (mensagem) {
		client.quit = true;
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'quit',
			data: mensagem
		});
	});

	socket.on('join', function (canais) {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'join',
			data: canais
		});
	});

	socket.on('part', function (data) {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'part',
			data: data
		});
	});


	socket.on('message', function (data) {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'message',
			data: data
		});
	});
	socket.on('whois', function (data) {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'whois',
			data: data
		});
	});

	socket.on('names', function (canal) {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'names',
			data: canal
		});
	});

	socket.on('nick', function (data) {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'nick',
			data: data
		});
	});

	socket.on('action', function (data) {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'action',
			data: data
		});
	});


	socket.on('motd', function () {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'motd'
		});
	});


	socket.on('list', function () {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'list'
		});
	});

	socket.on('notice', function (data) {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'notice',
			data: data
		});
	});

	socket.on('mode', function (data) {
		amqp.writeONQueue('irc', {
			id: client.id,
			command: 'mode',
			data: data
		});
	});
};
