module.exports = function (proxies, proxy) {
	let amqp = proxy.amqp;
	let socket = proxy.socket;

	function amqpMessageHandler(msg) {
		if (!msg) return;
		proxy.consumerTag = msg.fields.consumerTag;
		let message = JSON.parse(msg.content.toString());


		switch (message.type) {

			case 'error':
				socket.emit('proxy error', message.err);
				break;

			case 'irc event':
				socket.emit(message.event, message.data);
				break;

			case 'registration':
				registrationHandler(message.data);
				break;

			default:
				socket.emit('proxy error');
		}
	}

	function registrationHandler(info) {
		delete proxies[proxy.id];
		proxies[info.id] = proxy;

		proxy.id = info.id;
		proxy.queue = 'user_' + info.id;
		proxy.amqp.channel.assertQueue(proxy.queue, {durable: false});
		proxy.amqp.channel.consume(proxy.queue, amqpMessageHandler, {noAck: true});

		proxy.socket.emit('info inicial', info);
	}

	amqp.channel.consume(proxy.queue, amqpMessageHandler, {noAck: true});
};
