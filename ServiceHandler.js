const registrationHandler = require('./proxy/registrationHandler');
const comands = require('./proxy/commandsHandler');
const proxyConf = require('./configs/proxyConfig');
let clients = {};
let amqp = {
	lib: require('amqplib/callback_api'),
	connection: null,
	channel: null,
	writeONQueue: function (queue, message, sendOptions) {
		this.channel.assertQueue(queue, {durable: false});

		if (typeof message !== 'string')
			message = JSON.stringify(message);

		this.channel.sendToQueue(queue, Buffer.from(message), sendOptions);
	},
	sendIrcEvent: function (queue, event, data) {
		this.send(queue, {
			type: 'irc event',
			event: event,
			data: data
		});
	}
};

amqp.lib.connect(proxyConf.urlAMQP, function (err, conn) {
	if (err)
		return console.error(err);

	amqp.connection = conn;
	conn.createChannel(function (err, ch) {
		if (err)
			return console.error(err);

		amqp.channel = ch;
		registrationHandler(clients, amqp);
		comands(clients, amqp);
	});
});

